# Spool Adapter (work in progress)

Adaptateur de bobine de filament CCTREE pour Creality Ender 3.

Cet adaptateur permet d'ajuster le diamètre du trou de la bobine au diamètre du support de bobine d'origine de l'imprimante.
**En fonction de l'épaisseur de la bobine par rapport à la longueur du support, il peut éventuellement nécessiter un adaptateur tubulaire supplémentaire ou être collé à la bobine afin d'être maintenu en place.**

Ce projet est avant tout un POC pour tester l'enregistrements de projets FreeCAD sous forme de dossiers (format d'enregistrement possible sur la version #LinkStage3 de **RealThunder**)

En effet, l'enregistrement sous forme de dossier permet un meilleur versionning avec git (fichiers textes vs archive binaire)

Il n'est pas prévu de fournir le Spool Adapter en version STL ou même FCStd dans le repo.
En revanche, s'il s'avère possible de générer automatiquement un de ces formats avec gitlab-ci, ça peut être un bon complément à mon POC.

Il est possible qu'à l'avenir, le Spool Adapter devienne paramétrique pour gérer d'autres marques de bobines. Ca dépendra de mes futurs besoins.
